from teipublisher_api import __version__
from teipublisher_api.session import TEIPublisherSession
from decouple import config

ENDPOINT = config("ENDPOINT")
USERNAME = config("USERNAME")
PASSWORD = config("PASSWORD")
COLLECTION = config("COLLECTION")

tei = TEIPublisherSession(ENDPOINT, USERNAME, PASSWORD)


def test_session():
    assert isinstance(tei, TEIPublisherSession)


def test_version():
    assert __version__ == "0.1.0"


def test_fragment():

    result = tei.get_fragment(
        collection="doc",
        document="documentation.xml",
        odd="shakespeare.odd",
        xpath="//info[1]",
    )

    assert result.status_code == 200
    assert result.text.endswith("</info>\n</div>")


def test_fragment_without_xpath():
    result = tei.get_fragment(
        collection="doc",
        document="documentation.xml",
        odd="shakespeare.odd",
    )

    assert result.status_code == 200
    assert result.text.endswith("</article>\n</div>")
