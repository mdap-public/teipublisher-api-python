import requests
import logging
import ntpath

logging.basicConfig(level=logging.INFO)


class TEIPublisherSession:
    def __init__(self, endpoint, username, password):
        self.endpoint = endpoint
        self.username = username
        self.password = password
        self.session = requests.Session()

    def login(self):
        url = f"{self.endpoint}/login"
        logging.info("Logging in")
        return self.session.post(
            url, data={"user": self.username, "password": self.password}
        )

    def upload_file(self, filepath, collection):
        url = f"{self.endpoint}/upload/{collection}"
        with open(filepath, "rb") as f:
            logging.info(f"Uploading {filepath}")
            return self.session.post(url, files={"files[]": f})

    def recompile_odd(self, filepath, check_compiles="true"):
        url = f"{self.endpoint}/odd"
        logging.info("Recompiling ODD")
        return self.session.post(
            url, params={"odd": ntpath.basename(filepath), "check": check_compiles}
        )

    def logout(self):
        logging.info("Logging out")
        self.session.close()

    def get_transformed(self, collection, document, output_format, odd):
        """
        Retrieve transformed documents from the TeiPublisher API

        Arguments:
        collection: str -- collection containing the TEI document
        document: str -- name of TEI document
        output_format: str -- desired output format: one of pdf, html, epub
        odd: str -- ODD file used to transform the TEI
        """

        if output_format not in ["pdf", "html", "epub"]:
            raise Exception("output_format must be one of pdf, html or epub")

        url = f"{self.endpoint}/document/{collection}%2F{document}/{output_format}?"

        params = {"odd": odd, "wc": "true"}

        return self.session.get(url, params=params)

    def get_fragment(
        self,
        collection,
        document,
        odd,
        xpath=None,
        output_format="html",
    ):
        url = f"{self.endpoint}/parts/{collection}%2F{document}/{output_format}?"

        params = {
            "odd": odd,
            "view": "single",
            "xpath": xpath,
        }

        return self.session.get(url, params=params)
