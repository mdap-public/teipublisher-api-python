# TEI Publisher API

A simple python wrapper around the TEIPublisher API

## Installation

`pip install git+https://gitlab.unimelb.edu.au/mdap-public/teipublisher-api-python.git`

## Import

`from teipublisher_api.session import TEIPublisherSession`

## Example use

```
import glob
import os
from teipublisher_api.session import TEIPublisherSession

ENDPOINT = os.getenv("TEIPUBLISHER_ENDPOINT")
USERNAME = os.getenv("TEIPUBLISHER_USER")
PASSWORD = os.getenv("TEIPUBLISHER_PASSWORD")
COLLECTION = os.getenv("TEIPUBLISHER_UPLOAD_COLLECTION")

tei = TEIPublisherSession(ENDPOINT, USERNAME, PASSWORD)
files_to_import = (
    glob.glob("**/*.odd", recursive=True)
    + glob.glob("**/*.xml", recursive=True)
    + glob.glob("**/*.tei", recursive=True)
)

tei.login()
for file in files_to_import:
    tei.upload_file(file, COLLECTION)
    if file.endswith(".odd"):
        tei.recompile_odd(file)
tei.logout()
```
